package com.example.filedemo.service;

import com.example.filedemo.service.FTPProperties;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

@Component
public class FTPWriter  {

    private FTPProperties FTPProperties;
    protected FTPClient ftpClient;

    @Autowired
    public FTPWriter(@Autowired FTPProperties FTPProperties) {
        this.FTPProperties = FTPProperties;
    }

    @PostConstruct
    public void init() {
        if (this.FTPProperties.isAutoStart()) {
            this.open();
        }
    }

    public boolean open() {
        close();
        ftpClient = new FTPClient();
        boolean loggedIn = false;
        try {
            ftpClient.connect(FTPProperties.getServer(), FTPProperties.getPort());
            System.out.println("Connected to the server " + FTPProperties.getServer() + ":" + FTPProperties.getPort());
            loggedIn = ftpClient.login(FTPProperties.getUsername(), FTPProperties.getPassword());
            System.out.println("Logging in  " + loggedIn);
            if (FTPProperties.getKeepAliveTimeout() > 0)
                ftpClient.setControlKeepAliveTimeout(FTPProperties.getKeepAliveTimeout());
                ftpClient.changeWorkingDirectory("upload");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loggedIn;
    }

    public void close() {
        if (ftpClient != null) {
            try {
                ftpClient.logout();
                ftpClient.disconnect();
            } catch (Exception e) {
              e.printStackTrace();
            }
        }
    }

    public boolean loadFile(String remotePath, OutputStream outputStream) {
        try {
            return ftpClient.retrieveFile(remotePath, outputStream);
        } catch (IOException e) {
          e.printStackTrace();
            return false;
        }
    }


    public boolean saveFile(InputStream inputStream, String destPath, boolean append) {
        try {
            System.out.println("Trying to store a file to destination path " + destPath);
            if(append)
                return ftpClient.appendFile(destPath, inputStream);
            else
                return ftpClient.storeFile(destPath, inputStream);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean saveFile(String sourcePath, String destPath, boolean append) {
        System.out.println("Trying to store a file  " + sourcePath + "to destination path " + destPath);
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(new File(sourcePath));
        } catch (IOException e) {
            return false;
        }
        return this.saveFile(inputStream, destPath, append);
    }

    public boolean isConnected() {
        boolean connected = false;
        if (ftpClient != null) {
            try {
                connected = ftpClient.sendNoOp();
            } catch (Exception e) {
              e.printStackTrace();
            }
        }
        return connected;
    }
}
